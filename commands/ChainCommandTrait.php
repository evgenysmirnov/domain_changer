<?php


namespace common\commands;


trait ChainCommandTrait
{
    protected $commands = [];

    public function addNextCommand($command) {
        if (!is_array($command)) {
            $command = [$command];
        }
        foreach ($command as $item) {
            $this->commands[] = $item;
        }
    }

    public function handleNextCommands() {
        foreach ($this->commands as $command) {
            \Yii::$app->commandBus->handle($command);
        }
    }

}