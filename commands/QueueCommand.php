<?php


namespace common\commands;


use trntv\bus\interfaces\QueuedCommand as QueuedCommandInterface;
use trntv\bus\interfaces\SelfHandlingCommand;
use trntv\bus\middlewares\QueuedCommandTrait;
use yii\base\Object;
use yii\queue\Job;

abstract class QueueCommand extends Object implements SelfHandlingCommand, Job, QueuedCommandInterface
{
    use QueuedCommandTrait;
    use ChainCommandTrait;

    public function handle($command)
    {
    }

    public function execute($queue)
    {
        $this->handle(null);
    }
}