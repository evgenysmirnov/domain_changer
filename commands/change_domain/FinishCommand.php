<?php

namespace common\commands\change_domain;

use common\commands\QueueCommand;
use common\commands\SendEmailChangeDomainFinishCommand;
use common\models\Domain;
use Yii;

class FinishCommand extends QueueCommand
{
    use LogableTrait;
    use HasChangerTrait;

    public function init()
    {
        parent::init();
        $this->loadChanger();
    }

    public function handle($command) {
        Yii::trace("start " . static::class, 'domains');
        try {
            if ($this->changer->is_loaded) {
                Domain::changeActive($this->changer->new_id, $this->changer->old_id);
                
                $blocked_domain = Domain::findOne($this->changer->old_id);
                $active_domain = Domain::findOne($this->changer->new_id);
                $message = Yii::t('backend', 'Finish changing domain from {blocked_domain} to {active_domain} on {server_name} by {command_name}',
                    [
                        'blocked_domain'=> $blocked_domain->name,
                        'active_domain'=> $active_domain->name,
                        'server_name'=> $active_domain->server,
                        'command_name'=> static::class,
                    ]);

                Yii::warning($message, 'domains');
                Yii::$app->commandBus->handle(new SendEmailChangeDomainFinishCommand(compact('message', 'blocked_domain', 'active_domain')));

                $this->changer->delete();
            }
            return true;
        }
        catch(\Exception $ex) {
            $this->log_exception($ex);
            return false;
        }
    }

}