<?php


namespace common\commands\change_domain;


use common\commands\change_domain\models\Changer;

trait HasChangerTrait
{
    public $changer_id;

    /**
     * @var Changer
     */
    protected $changer;

    public function loadChanger()
    {
        $this->changer = Changer::loadById($this->changer_id);
    }
}