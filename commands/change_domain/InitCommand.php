<?php

namespace common\commands\change_domain;

use common\commands\change_domain\models\Changer;
use common\commands\QueueCommand;
use common\models\Domain;
use Yii;

class InitCommand extends QueueCommand
{
    use LogableTrait;

    public $old_domain_id;
    public $new_domain_id;

    /**
     * @var Domain
     */
    protected $old_domain;

    /**
     * @var Domain
     */
    protected $new_domain;

    public function init()
    {
        parent::init();

        $this->new_domain = Domain::findOne($this->new_domain_id);
        $this->old_domain = Domain::findOne($this->old_domain_id);

        if (!$this->old_domain || !$this->new_domain) {
            Yii::error('Not found old (' . $this->old_domain_id . ') or new (' . $this->new_domain_id . ') domain by ' . static::class, 'domains');
            \Yii::$app->end();
        }
    }


    public function handle($command) {
        Yii::trace("start " . static::class, 'domains');
        try {
            if (!empty($landings = Yii::$app->voluum->getLandingsByBaseDomain($this->old_domain->name))) {
                $changer = Changer::create([
                    "count"=>count($landings),
                    "old_id"=>$this->old_domain->id,
                    "new_id"=>$this->new_domain->id,
                    'new_domain' => $this->new_domain->name
                ]);
                foreach ($landings as $landing) {
                    Yii::$app->commandBus->handle(new UpdateLandingCommand(['landing_id' => $landing['landerId'], 'changer_id' => $changer->id]));
                }
            }
            else {
                Yii::warning('Nothing for change  by ' . static::class, 'domains');
            }
        }
        catch(\Exception $ex) {
            $this->log_exception($ex, "new_domain:{$this->new_domain}, old_domain:{$this->old_domain}");
            return false;
        }

        return true;
    }

}