<?php


namespace common\commands\change_domain;


trait LogableTrait
{
    public function log_exception(\Exception $ex, $message = '')
    {
        \Yii::error(implode(' ', array_filter('Exception by ' . static::class, $ex->getMessage(), $message)), 'domains');
    }
}