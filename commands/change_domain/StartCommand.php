<?php


namespace common\commands\change_domain;

use common\models\Domain;
use Yii;
use yii\base\Object;
use trntv\bus\interfaces\SelfHandlingCommand;


class StartCommand extends Object implements SelfHandlingCommand
{
    use LogableTrait;

    /**
     * @var Domain
     */
    public $domain;

    public function init()
    {
        parent::init();

        if (is_string($this->domain)) {
            $domain = $this->domain;
            if (empty($this->domain = Domain::getForChangingByName($domain))) {
                Yii::error("Can't find blocked domain " . $domain . ' by ' . static::class, 'domains');
            }
        }

        if (!($this->domain instanceof Domain)) {
            $this->domain = null;
        }
    }

    public function handle($command)
    {
        if (empty($this->domain)) {
            return true;
        }

        Yii::warning("Call domain changing " . $this->domain->name . ' by ' . static::class, 'domains');
        $new_domain = Domain::find()->available($this->domain->server)->one();
        if ($new_domain) {
            Yii::$app->commandBus->handle(new InitCommand(['old_domain_id' => $this->domain->id, 'new_domain_id' => $new_domain->id]));
        }
    }

}