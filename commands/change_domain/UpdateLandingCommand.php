<?php

namespace common\commands\change_domain;

use common\commands\QueueCommand;
use common\helpers\ArrayHelper;
use common\helpers\UrlHelper;
use Yii;

class UpdateLandingCommand extends QueueCommand
{
    use HasChangerTrait;
    use LogableTrait;

    public $landing_id;

    public function init()
    {
        parent::init();
        $this->loadChanger();
    }

    public function handle($command) {
        Yii::trace("start " . static::class, 'domains');
        try {
            $landing = Yii::$app->voluum->getLanding($this->landing_id);
            if (
                !empty($url = ArrayHelper::get($landing, 'url' , '')) &&
                !empty($this->changer->new_domain)
            ) {
                if (!empty($base_domain = UrlHelper::base_domain($url))) {
                    $url = str_replace($base_domain, $this->changer->new_domain, $url);
                    $update_attributes = array_merge(
                        ArrayHelper::only($landing, ['namePostfix']),
                        ['url'=>$url]
                    );
                    Yii::$app->voluum->updateLanding($this->landing_id, $update_attributes);
                }
                if ($this->changer->decrementCount()) {
                    Yii::$app->commandBus->handle(new FinishCommand(['changer_id' =>$this->changer_id]));
                }
            }
        }
        catch(\Exception $ex) {
            $this->log_exception($ex, "landing_id:{$this->landing_id}");
            return false;
        }

        return true;
    }

}