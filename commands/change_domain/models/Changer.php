<?php


namespace common\commands\change_domain\models;


use yii\base\Model;

/**
 * Class Changer
 * @package common\commands\change_domain\models
 * @property string $id
 * @property string $key
 * @property \Predis\Client $redis
 */
class Changer extends Model
{
    protected $id;

    /**
     * @var integer
     */
    public $new_id;

    /**
     * @var string
     */
    public $new_domain;

    /**
     * @var integer
     */
    public $old_id;

    /**
     * @var integer
     */
    public $count;

    public $is_loaded;

    /**
     * @return mixed
     */
    public function getId()
    {
        if (!isset($this->id)) {
            $this->id = uniqid();
        }
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getKey()
    {
        return 'changer_cd__' . $this->getId();
    }

    public function init()
    {
        parent::init();
    }
    
    public function rules()
    {
        return array_merge(
            parent::rules(),
            [
                [['new_id', 'old_id', 'count', 'new_domain', 'is_loaded'], 'safe']
            ]
        );
    }

    public static function loadById($id)
    {
        $load = new static();
        $load->setId($id);
        $load->is_loaded = false;
        if (!empty($attributes = $load->redis->hGetAll($load->key))) {
            $load->attributes = array_merge(
                $attributes,
                ['is_loaded'=>true]
            );
        }
        return $load;
    }

    public function delete()
    {
        $this->redis->del($this->key);
    }

    /**
     * @return \Predis\Client
     */
    public function getRedis()
    {
        return \Yii::$app->redis;
    }

    /**
     * @param array $attr
     * @return null|static
     */
    public static function create($attr = []) {
        $obj = new static($attr);
        return $obj->save() ? $obj : $obj;
    }

    public function save()
    {
        $this->redis->hMSet($this->key, $this->attributes);
    }

    public function decrementCount()
    {
        $this->count = $this->redis->hIncrBy($this->key, 'count', -1);
        return $this->count <= 0;
    }
}