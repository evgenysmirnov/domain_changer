<?php
namespace common\components\voluum;

use cheatsheet\Time;
use common\components\voluum\exceptions\AuthException;
use common\components\voluum\exceptions\Exception;
use common\helpers\ArrayHelper;
use common\helpers\UrlHelper;
use Yii;
use yii\base\InvalidConfigException;
use yii\base\Object;
use yii\di\Instance;
use yii\httpclient\Client as HttpClient;
use yii\httpclient\Response;
use yii\redis\Connection as RedisClient;

use common\components\voluum\exceptions\LandingExistsException;

/**
 * Class VoluumService
 * @package common\components\voluum
 * @property string $key_token
 * @property string $key_token_locker
 * @property HttpClient $httpclient
 */
class VoluumService extends Object
{
    const BASE_HOST = 'https://api.voluum.com/';
    const AUTH_URL = 'auth/access/session';
    const LANDER_URL = 'lander';
    const RESTORE_URL = self::LANDER_URL . '/restore';
    const REPORT_URL = 'report';

    const LIMIT_BATCH = 1000;

    const TOKEN_EXPIRATION = Time::SECONDS_IN_AN_HOUR;

    /**
     * @var string
     */
    public $access_id;
    /**
     * @var string
     */
    public $access_key;
    /**
     * @var string
     */
    public $key_prefix;

    /**
     * @var RedisClient
     */
    public $redis = 'redis';

    /**
     * @var HttpClient
     */
    protected $_httpclient;

    /**
     * @var string
     */
    protected $token;

    //================================================================================

    // getters/setters
    //================================================================================

    /**
     * @return HttpClient
     */
    public function getHttpclient()
    {
        if (!isset($this->_httpclient)) {
            $this->_httpclient = new HttpClient([
                'baseUrl' => static::BASE_HOST,
                'requestConfig' => [
                    'options' => [
                        'timeout' => 5
                    ],
                ],
                'responseConfig' => [
                    'format' => HttpClient::FORMAT_JSON
                ]
            ]);
        }

        if ($this->token) {
            ArrayHelper::set($this->_httpclient->requestConfig, "headers", ['cwauth-token'=>$this->token]);
        }

        return $this->_httpclient;
    }

    /**
     * @return string
     */
    public function getKey_token()
    {
        return $this->key_prefix . 'token';
    }

    /**
     * @return string
     */
    public function getKey_token_locker()
    {
        return $this->key_prefix . 'token_locker';
    }

    /**
     * @return string
     */
    public function getToken()
    {
        if (!isset($this->token)) {
            $this->token = $this->redis->get($this->key_token);
        }
        return $this->token;
    }

    /**
     * @param string $token
     */
    public function setToken($token)
    {
        $this->token = $token;
        if (!empty($token)) {
            $this->redis->setex($this->key_token, static::TOKEN_EXPIRATION, $token);
        }
        else {
            $this->redis->del($this->key_token);
        }
    }

    /**
     * @return string
     */
    public function getDefaultKeyPrefix()
    {
        return 'voluum.' . substr(md5(implode("", array_filter([$this->access_id, Yii::$app->id]))), 0, 5);
    }

    protected function getAuthData()
    {
        return ['accessId'=>$this->access_id, 'accessKey'=>$this->access_key];
    }

    protected function getReportParams($params = [])
    {
        return array_merge(
            [
                'from' => date("Y-m-d", time() - Time::SECONDS_IN_A_DAY) . 'T00:00:00Z',
                'to' => date("Y-m-d", time() + Time::SECONDS_IN_A_DAY) . 'T00:00:00Z',
                'tz' => 'UTC',
                'sort' => 'visits',
                'direction' => 'desc',
                'groupBy' => 'lander',
                'offset' => 0,
                'include' => 'ALL',
            ],
            $params
        );
    }

    //================================================================================
    // events
    //================================================================================
    /**
     * @throws InvalidConfigException
     * @return void
     */
    public function init()
    {
        $this->redis = Instance::ensure($this->redis, RedisClient::class);
        $this->key_prefix = $this->key_prefix ?: $this->getDefaultKeyPrefix();

        parent::init();
    }

    public function __call($name, $params)
    {
        if (method_exists($this, $name)) {
            return $this->request($name, $params);
        }

        parent::__call($name, $params);
    }

    //================================================================================
    // auth
    //================================================================================
    /**
     * @return bool
     */
    public function check_auth()
    {
        return !empty($this->getToken());
    }

    public function auth($reset = false) {
        if (!$reset && $this->check_auth()) {
            return true;
        }

        if (!$this->redis->setnx($this->key_token_locker, '1')) {
            sleep(1);
            return $this->auth();
        }
        $this->redis->expire($this->key_token_locker, 5);

        try
        {
            $this->parseResponseAuth($this->httpclient->post(static::AUTH_URL, $this->getAuthData())->setFormat(HttpClient::FORMAT_JSON)->send());
            return true;
        }
        finally {
            $this->redis->del($this->key_token_locker);
        }
    }

    protected function parseResponseAuth(Response $response)
    {
        if ($response->isOk && !empty($token = ArrayHelper::get($response->data, 'token'))) {
            $this->setToken($token);
            return true;
        }
        throw new AuthException(ArrayHelper::get($response->data, 'message', "Some problems with Voluum auth"));
    }

    //================================================================================
    // utils
    //================================================================================
    protected function parseResponse(Response $response) {
        if ($response->isOk) {
            return $response->data;
        }

        switch ($response->getStatusCode()) {
            case "401" :
                throw new AuthException('Token has been expired.');
            case "400" :
                if (strtoupper(ArrayHelper::get($response->data, 'error.code')) == 'NAME_NOT_UNIQUE') {
                    throw new LandingExistsException();
                }
                throw new Exception(ArrayHelper::get($response->data, 'error.description', 'Bad request'));
        }

        throw new Exception('Unknown status code: ' . $response->getStatusCode());
    }

    public function request($action, $attr = []) {
        if (!$this->auth()) {
            return false;
        }
        try {
            return call_user_func_array([$this, $action], $attr);
        }
        catch (AuthException $e) {
            $this->setToken(null);
            $this->auth(true);
            return $this->request($action, $attr);
        }
    }

    //================================================================================
    // api actions
    //================================================================================
    /**
     * @return array
     */
    protected function getLandings()
    {
        return ArrayHelper::get(
            $this->parseResponse($this->httpclient->get(static::LANDER_URL, ['includeDeleted'=>'true'])->send()),
            "landers",
            []
        );
    }

    protected function getLanding($id)
    {
        return $this->parseResponse($this->httpclient->get(static::LANDER_URL . '/' . $id)->send());
    }

    protected function createLanding($landing)
    {
        $_default_attr = ['namePostfix'=>'', 'url'=>'', 'numberOfOffers'=>1, 'tags'=>[]];
        $attr = array_merge($_default_attr,  $landing);

        return $this->parseResponse($this->httpclient->post(static::LANDER_URL, json_encode($attr), ['content-type' => 'application/json'])->send());
    }

    protected function updateLanding($id, $attr)
    {
        return $this->parseResponse(
            $this->httpclient->put(static::LANDER_URL . '/' . $id, json_encode(ArrayHelper::except($attr, ['id'])), ['content-type' => 'application/json'])->send()
        );
    }

    protected function restoreLanding($id)
    {
        $data = ['ids' => (array) $id];
        return
            strtoupper(
                ArrayHelper::get(
                    $this->parseResponse($this->httpclient->post(static::RESTORE_URL, json_encode($data), ['content-type' => 'application/json'])->send()),
                'status', "")
            ) == 'SUCCESSFUL';
    }

    protected function deleteLanding($id)
    {
        $data = ['ids' => (array) $id];
        return
            strtoupper(
                ArrayHelper::get(
                    $this->parseResponse($this->httpclient->delete(static::LANDER_URL, json_encode($data), ['content-type' => 'application/json'])->send()),
                'status', "")
            ) == 'SUCCESSFUL';
    }

    protected function getLandingsByFilter($params = []) {
        $offset = 0;
        $landings = [];
        while (true) {
            if ($params && is_string($params)) {
                $params = ['filter'=>$params];
            }
            $params = array_merge($params, ['offset' => $offset, 'limit' => static::LIMIT_BATCH]);
            $response = $this->parseResponse(
                $this->httpclient->get(static::REPORT_URL, $this->getReportParams($params))->send()
            );
            $landings = array_merge($landings, array_map(function($row) {
                return ArrayHelper::only($row, [
                    'created', 'deleted', 'landerId', 'landerName', 'landerUrl', 'numberOfOffers', 'updated'
                ]);
            }, ArrayHelper::get($response, 'rows', [])));

            if (ArrayHelper::get($response, 'totalRows', 0) * 1 <= $offset + static::LIMIT_BATCH) {
                return $landings;
            } else {
                $offset += static::LIMIT_BATCH;
            }
        }
    }

    protected function getLandingsByBaseDomain($base_domain) {
        $base_domain = strtolower($base_domain);
        return array_filter(
            $this->getLandingsByFilter(['filter'=>$base_domain, 'include' => "ACTIVE"]),
            function($landing) use($base_domain) {
                return $base_domain == UrlHelper::base_domain(ArrayHelper::get($landing, 'landerUrl'));
            }
        );
    }

    protected function getLandingByName($name)
    {
        return ArrayHelper::first($this->getLandingsByFilter(['filter'=>$name]));
    }
}